app.component('recipe-card',{
    props:{
        image:{
            type: String
        },
        category:{
            type: String,
            default: "default category"
        },
        name:{
            type: String,
            default: "default name"
        },
        description:{
            type: String,
            default: "default description"
        },
        time:{
            type: String,
            default: "default time"
        },
        level:{
            type: String,
            default: "default level"
        },
        likes:{
            type: Number,
            default: 1
        },
        index:{
            type: Number
        }
    },
 methods:{
    onClickLike(){
        //console.log("LIKE");
        this.$emit('recipelike', this.index);
        //this.recipe_likes++;
    },
    onClickUnlike(){
        //console.log("UNLIKE");
        this.$emit('recipeunlike', this.index);

    },
    onClickViewRecipe(){
        console.log("VIEW");
        this.$emit('recipedetails', this.index);
        //this.$test.emit('foo',"works!");
    },
 },
    template:
    /*html*/ 
    `
    

    <div class="card col-4 mt-4" style="width: 23rem;">
        <img v-bind:src="image" class="card-img-top mt-2 " alt="...">
        <div class="card-body">
          <h5 class="card-title">{{ name }}</h5>
          <h5 class="mt-3">Tiempo aproximado de preparacion : 5 min</h5>
          <div class="row ">
                    <div class="col ">
                        <button  class="btn btn-outline-danger mt-4  " v-on:click="onClickLike(index)">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24" fill="currentColor" class="bi bi-heart-fill " viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                              </svg>
                          </button>
                          {{ likes }}
                    </div>

                    <div class="col mt-4 pt-2">
                    
                    </div>

          <a href="../proyectorecetas/detalleRecetas.html" class="btn verReceta d-flex justify-content-center mt-5 ">Ver
            Receta</a>
        </div>
      </div>


        </div>

        
    

`
})