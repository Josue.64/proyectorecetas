app.component('subir-recetas',{




    template:
       /*html*/ 
       `
       <div class="row justify-content-center">
       <div class="col-6 mt-5 ">
           <h2>Sube tu propia receta</h2>
           <div class="container mt-5">

               <button class="btn btnAbrirformulario" type="button" data-toggle="collapse"
                   data-target="#formulario" aria-expanded="false" aria-controls="formulario">
                   Abrir formulario
               </button>
               <div class="collapse mt-3" id="formulario">
                   <form class="formularios">
                       <div class="form-group">
                           <label for="nombre">Nombre de la receta:</label>
                           <input type="text" class="form-control" id="">
                       </div>
                       <div class="form-group">
                           <label for="correo">Categoria:</label>
                           <input type="text" class="form-control" id="">
                       </div>
                       <div class="form-group">
                           <label for="mensaje">Tiempo de preparacion:</label>
                           <input type="text" class="form-control" id="">
                       </div>
                       <div class="form-group">
                           <label for="mensaje">Como se prepara:</label>
                           <textarea class="form-control" id="" rows="3"></textarea>
                       </div>
                       <div class="mt-3 mb-3">
                           <select class="form-select" aria-label="Default select example">
                               <option selected>Dificultad</option>
                               <option value="1">Facil</option>
                               <option value="2">Intermedio</option>
                               <option value="3">Avanzado</option>
                               
                           </select>
                       </div>

                       <button type="submit" class="btn btnSubirImg ">Click aquí para subir imagen</button>
                       <button type="submit" class="btn btnSubirReceta fw-bold ms-2">Subir receta</button>
                   </form>
               </div>

           </div>
       </div>
           
   </div>
   
       
       
   
   `
   })